use crate::{util::ui_util::AppStyle, Context, Message};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, env, fs, path::Path};

pub type KeyConfig = HashMap<Context, HashMap<String, Message>>;
pub type ThemeConfig = HashMap<AppStyle, String>;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Config {
    pub host: String,
    pub port: String,
    pub event_threads: u32,
    pub keys: KeyConfig,
    pub theme: ThemeConfig,
}

impl Default for Config {
    fn default() -> Self {
        Self::new()
    }
}

impl Config {
    pub fn new() -> Self {
        env::var("XDG_CONFIG_HOME")
            .or_else(|_| env::var("HOME").map(|home| format!("{}/.config", home)))
            .ok()
            .and_then(|config_home| {
                let config_dir = Path::new(&config_home).join("mpc-rs");
                fs::create_dir_all(config_dir.clone())
                    .ok()
                    .and(fs::read_to_string(config_dir.join("config.ron")).ok())
            })
            .and_then(|string| ron::from_str(&string).ok())
            .unwrap_or_else(|| ron::from_str(include_str!("config.default.ron")).unwrap())
    }
}
