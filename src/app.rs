use crate::{
    data_types::*,
    model::{input, library, playlists, progress_bar, queue, volume, Model},
    util::event::{Event, Events},
    view::View,
    AppBackend, AppView, Config, MPDConnection,
};
use std::{
    error::Error,
    sync::{
        atomic::{AtomicBool, Ordering},
        mpsc, Condvar, Mutex, MutexGuard,
    },
    collections::HashMap,
};
use termion::{input::MouseTerminal, raw::IntoRawMode, screen::AlternateScreen, event::Key};
use tui::{backend::TermionBackend, terminal::Frame, Terminal};

/**
 * Rules for aquiring a lock on connection and/or model to prevent deadlocks:
 * - always lock connection before locking model
 * - if a lock on model has be aqiired, release the lock before locking connection
 * Or simply: never keep a lock on model
 */
pub struct App {
    model: Mutex<Model>,
    model_cvar: Condvar,
    connection: Mutex<MPDConnection>,
    config: Config,
    view: Mutex<AppView>,
    storage: Mutex<HashMap<String, Selector>>,
    quit: AtomicBool,
    rx: Mutex<mpsc::Receiver<Message>>,
    tx: Mutex<mpsc::Sender<Message>>,
}

impl Default for App {
    fn default() -> Self {
        Self::new()
    }
}

impl App {
    pub fn new() -> Self {
        let config = Config::new();
        let (tx, rx) = mpsc::channel();

        Self {
            model: Mutex::new(Model::new(&config)),
            model_cvar: Condvar::new(),
            connection: Mutex::new(MPDConnection::new(&config)),
            view: Mutex::new(AppView::new()),
            config,
            storage: Mutex::new(HashMap::new()),
            quit: AtomicBool::new(false),
            rx: Mutex::new(rx),
            tx: Mutex::new(tx),
        }
    }

    pub fn render_loop(&self) -> Result<(), Box<dyn Error>> {
        let mut terminal = {
            let stdout = std::io::stdout().into_raw_mode()?;
            let stdout = MouseTerminal::from(stdout);
            let stdout = AlternateScreen::from(stdout);
            let backend = TermionBackend::new(stdout);
            Terminal::new(backend)?
        };

        let mut model = self.model();

        loop {
            terminal.draw(|f: &mut Frame<AppBackend>| {
                self.view()
                    .view(&mut *model, &self.config().theme, f, f.size())
            })?;

            model = self.wait(model);

            if self.has_quit() {
                break;
            }
        }

        Ok(())
    }

    pub fn input_loop(&self) {
        let events = Events::new();

        loop {
            let event = events.next().unwrap();

            if let Event::Input(input) = event {
                let mut model = self.model();
                let mut status = false;

                model.error_message = None;

                match model.input_state {
                    InputState::KeyCommands => {
                        if let Some(message) = model
                            .keys
                            .get_message(&model.current_content().input_context(), input)
                        {
                            status = true;
                            self.send_message(message);
                        }
                        model.keys.set_status(status, input);
                    }
                    InputState::TextInput => {
                        if let Some(message) = model
                            .keys
                            .get_message(&[Context::Input], input)
                        {
                            status = true;
                            self.send_message(message);
                        } else {
                            match input {
                                Key::Char(c) => model.input.type_char(c),
                                Key::Backspace => {
                                    model.input.pop_char();
                                }
                                _ => {}
                            }
                        }
                        model.keys.set_status(status, input);
                    }
                }
            }

            self.update_view();
            if self.has_quit() {
                break;
            }
        }
    }

    pub fn send_message(&self, message: Message) {
        if message == Message::Quit {
            for _ in 0..self.config().event_threads {
                let _ = self.tx.lock().unwrap().send(message.clone());
            }
        } else {
            let _ = self.tx.lock().unwrap().send(message);
        }
    }

    pub fn rx(&self) -> MutexGuard<mpsc::Receiver<Message>> {
        self.rx.lock().unwrap()
    }

    pub fn has_quit(&self) -> bool {
        self.quit.load(Ordering::Relaxed)
    }

    pub fn quit(&self) {
        self.quit.store(true, Ordering::Relaxed);
    }

    pub fn config(&self) -> &Config {
        &self.config
    }

    pub fn connection(&self) -> MutexGuard<MPDConnection> {
        self.connection.lock().unwrap()
    }

    pub fn model(&self) -> MutexGuard<Model> {
        self.model.lock().unwrap()
    }

    pub fn view(&self) -> MutexGuard<AppView> {
        self.view.lock().unwrap()
    }

    pub fn wait<'m>(&self, model: MutexGuard<'m, Model>) -> MutexGuard<'m, Model> {
        self.model_cvar.wait(model).unwrap()
    }

    pub fn update_view(&self) {
        self.model_cvar.notify_one()
    }

    pub fn start_loading(&self) {
        self.model().loading += 1;
        self.update_view();
    }

    pub fn stop_loading(&self) {
        self.model().loading -= 1;
        self.update_view();
    }

    pub fn seek(&self, offset: i32) {
        let mut connection = self.connection();
        if let Ok(status) = connection.status() {
            if let Some(elapsed) = status.elapsed {
                let new_pos = elapsed.num_seconds() + (offset as i64);
                let _ = connection.rewind(new_pos);
            }
        }
    }

    pub fn store_variable(&self, name: String, selector: Selector) {
        self.storage.lock().unwrap().insert(name, selector);
    }

    pub fn load_variable(&self, name: String) -> Option<Selector> {
        self.storage.lock().unwrap().get(&name).cloned()
    }

    pub fn handle_message(&self, message: Message) -> Result<(), Box<dyn Error>> {
        log!("Handling message: {:?}", &message);

        use ComponentSelector::*;
        use Message::*;
        use Selector::*;

        // Parse message
        let message = message.parse();

        // Message chaining
        let (message, next) = if let Chain(mut vec) = message {
            if !vec.is_empty() {
                (vec.remove(0).parse(), Some(Chain(vec)))
            } else {
                return Ok(());
            }
        } else {
            (message, Option::None)
        };

        // Parallel messages
        if let Parallel(vec) = message {
            vec.iter().for_each(|msg| self.send_message(*msg.clone()));
            return Ok(());
        }

        // Resolve input selector
        if let Some(Input(name)) = message.selector() {
            {
                let mut model = self.model();
                model.input.handle_message(name, &message);
                model.input_state = InputState::TextInput;
            }

            self.update_view();
        } else {
            let message = if let Some(Var(name)) = message.selector() {
                if let Some(selector) = self.load_variable(name.clone()) {
                    message.replace_selector(selector)
                } else {
                    message
                }
            } else { message };

            //log!("Handling message: {:?}", &message);
            self.start_loading();

            let mut handled = true;

            match &message {
                Update(All) => {
                    progress_bar::update(self);
                    library::update(self);
                    queue::update(self);
                    playlists::update(self);
                    volume::update(self);
                }
                Update(Player) => progress_bar::update(self),
                Update(Library) => library::update(self),
                Update(Queue) => queue::update(self),
                Update(Playlists) => playlists::update(self),
                Update(Volume) => volume::update(self),

                Rescan => {
                    self.connection().rescan()?;
                }
                Seek(Number(offset)) => self.seek(*offset),
                TogglePause => self.connection().toggle_pause()?,
                SetTab(TabSelector::Next) => self.model().tabs.next(),
                SetTab(TabSelector::Prev) => self.model().tabs.previous(),
                SetTab(TabSelector::Tab(tab)) => self.model().tabs.set(tab.index()),
                ChangeVolume(Number(v)) => volume::change_volume(self, *v as i8),
                SetVolume(Number(v)) => volume::set_volume(self, *v as i8),
                CancelInput => {
                    let model = &mut*self.model();
                    model.input.clear();
                    model.input_state = InputState::KeyCommands;
                },
                ConfirmInput => {
                    let model = &mut*self.model();
                    let message = model
                        .input
                        .return_msg
                        .replace_selector(Selector::Text(model.input.buffer.clone()));
                    self.send_message(message);
                    model.input.clear();
                    model.input_state = InputState::KeyCommands;
                },
                TypeInput(Text(s)) => self.model().input.type_str(s),
                PopInputChar => self.model().input.pop_char(),
                Sleep(Number(s)) if *s >= 0 => std::thread::sleep(std::time::Duration::from_secs(*s as u64)),
                Store(name, s) => self.store_variable(name.clone(), s.clone()),
                Quit => self.quit(),
                _ => { handled = false; },
            }

            let current_content = self.model().current_content();

            handled = handled || match current_content {
                TabContent::Library => self.handle_library_message(&message),
                TabContent::Queue => self.handle_queue_message(&message),
                TabContent::Playlists => self.handle_playlists_message(&message),
            };

            if !handled {
                let error = format!("Invalid message: {:?}", &message);
                log!(&error);
                self.model().error_message = Some(error);
            }

            self.stop_loading();
        }

        if let Some(next) = next {
            self.send_message(next);
        }

        Ok(())
    }

    fn handle_library_message(&self, message: &Message) -> bool {
        use Message::*;
        use Selector::*;

        match message {
            MoveCursor(s) => self.model().library.table.select(s),
            QueueAdd(ListEntry(s)) => library::add_to_queue(self, s),
            Play(ListEntry(s)) => library::play(self, s),
            ListExpand(s) => library::state_down(self, s),
            ListCollapse => library::state_up(self),
            _ => return false
        }

        true
    }

    fn handle_queue_message(&self, message: &Message) -> bool {
        use Message::*;
        use Selector::*;

        match message {
            MoveCursor(s) => self.model().queue.table.select(s),
            Play(ListEntry(s)) => queue::play(self, s),
            _ => return false
        }

        true
    }

    fn handle_playlists_message(&self, message: &Message) -> bool {
        use Message::*;
        use Selector::*;

        match message {
            MoveCursor(s) => self.model().playlists.table.select(s),
            Play(ListEntry(s)) | LoadPlaylist(ListEntry(s)) => playlists::play(self, s),
            SavePlaylist(Text(name)) => playlists::save(self, name),
            _ => return false
        }

        true
    }
}
