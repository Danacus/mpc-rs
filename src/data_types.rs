use serde::{Deserialize, Serialize};
use strum_macros::EnumIter;

pub use crate::model::TabContent;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, EnumIter, Serialize, Deserialize)]
pub enum Context {
    Global,
    List,
    Queue,
    Playlists,
    Library,
    Volume,
    Input,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum ListEntrySelector {
    Selected,
    First,
    Last,
    Prev,
    Next,
    Nth(usize),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum TabSelector {
    Prev,
    Next,
    Tab(TabContent),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum ComponentSelector {
    Player,
    Library,
    Queue,
    Playlists,
    Volume,
    All,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum Selector {
    /// A string value
    Text(String),

    /// Number,
    Number(i32),

    /// Refers to an entry in the list
    ListEntry(ListEntrySelector),

    /// Ask the user for input
    Input(String),

    /// Use a stored variable
    Var(String),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum Message {
    /// Toggle play/pause
    TogglePause,

    /// Go to the given tab
    SetTab(TabSelector),

    /// Move cursor in a list
    MoveCursor(ListEntrySelector),

    /// Play a song, album or playlist
    Play(Selector),

    /// Load a playlist in the queue
    LoadPlaylist(Selector),

    /// Save the current queue as a playlist with the given name
    SavePlaylist(Selector),

    /// Add a song, album or playlist to the queue
    QueueAdd(Selector),

    /// Expand a list entry
    ListExpand(ListEntrySelector),

    /// Move up a level in a nested list
    ListCollapse,

    /// Change the volume by adding the given value to the current volume
    ChangeVolume(Selector),

    /// Set the volume to the given value
    SetVolume(Selector),

    /// Rescan files and update MPD database
    Rescan,

    /// Seek to a certain point in the current song
    Seek(Selector),

    /// Update the given component of the program
    Update(ComponentSelector),

    /// Run a command (message)
    Command(Selector),

    /// Exit input mode
    CancelInput,

    /// Confirm input
    ConfirmInput,

    /// Type text in an input field
    TypeInput(Selector),

    /// Remove the last character of the input field
    PopInputChar,

    /// Wait until input mode is exited
    WaitInput,

    /// Make the current event thread sleep
    Sleep(Selector),

    /// Store a value in a variable
    Store(String, Selector),

    /// Do nothing
    None,

    /// Quit
    Quit,

    /// Chain two messages, this makes sure they will run in sequence
    Chain(Vec<Box<Message>>),

    /// Run two messages in parallel
    Parallel(Vec<Box<Message>>),
}

impl Message {
    pub fn selector(&self) -> Option<&Selector> {
        use Message::*;

        match self {
            Play(s) => Some(s),
            LoadPlaylist(s) => Some(s),
            SavePlaylist(s) => Some(s),
            QueueAdd(s) => Some(s),
            ChangeVolume(s) => Some(s),
            SetVolume(s) => Some(s),
            Seek(s) => Some(s),
            Command(s) => Some(s),
            TypeInput(s) => Some(s),
            Sleep(s) => Some(s),
            Store(_, s) => Some(s),
            _ => Option::None,
        }
    }

    pub fn replace_selector(&self, s: Selector) -> Message {
        use Message::*;

        match self {
            Play(_) => Play(s),
            LoadPlaylist(_) => LoadPlaylist(s),
            SavePlaylist(_) => SavePlaylist(s),
            QueueAdd(_) => QueueAdd(s),
            ChangeVolume(_) => ChangeVolume(s),
            SetVolume(_) => SetVolume(s),
            Seek(_) => Seek(s),
            Command(_) => Command(s),
            TypeInput(_) => TypeInput(s),
            Sleep(_) => Sleep(s),
            Store(n, _) => Store(n.clone(), s),
            _ => self.clone(),
        }
    }

    pub fn parse(&self) -> Message {
        use Message::*;
        use Selector::*;

        match self {
            SetVolume(Text(vol_str)) => vol_str
                .parse()
                .map(|v| SetVolume(Number(v)))
                .unwrap_or(self.clone()),
            ChangeVolume(Text(vol_str)) => vol_str
                .parse()
                .map(|v| ChangeVolume(Number(v)))
                .unwrap_or(self.clone()),
            Sleep(Text(sleep_str)) => sleep_str
                .parse()
                .map(|n| ChangeVolume(Number(n)))
                .unwrap_or(self.clone()),
            Seek(Text(seek_str)) => seek_str
                .parse()
                .map(|s| Seek(Number(s)))
                .unwrap_or(self.clone()),
            Command(Text(text)) => ron::from_str::<Message>(text)
                .map(|msg| msg.parse())
                .unwrap_or(self.clone()),
            _ => self.clone(),
        }
    }
}

#[derive(Debug)]
pub enum InputState {
    TextInput,
    KeyCommands,
}
