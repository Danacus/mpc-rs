#![allow(dead_code)]
#![allow(unreachable_code)]

#[macro_use]
mod util;
mod app;
mod config;
mod data_types;
mod model;
mod view;

use crate::{
    app::App,
    data_types::*,
    model::*,
    util::{keys::Keys, log::log_init},
    view::AppView,
};
use backtrace::Backtrace;
use config::{Config, ThemeConfig};
use mpd::{
    idle::{Idle, Subsystem},
    Channel,
};
use std::{error::Error, panic, panic::PanicInfo};
pub use util::MPDConnection;

use std::{sync::Arc, thread};

use termion::{input::MouseTerminal, raw::RawTerminal, screen::AlternateScreen};
use tui::backend::TermionBackend;

pub type AppBackend = TermionBackend<AlternateScreen<MouseTerminal<RawTerminal<std::io::Stdout>>>>;

pub fn get_subsytems() -> Vec<Subsystem> {
    vec![
        Subsystem::Message,
        Subsystem::Player,
        Subsystem::Queue,
        Subsystem::Database,
        Subsystem::Mixer,
        Subsystem::Player,
    ]
}

pub fn subsystem_messages(subsystem: Subsystem) -> Vec<Message> {
    use ComponentSelector::*;
    use Message::*;

    match subsystem {
        Subsystem::Player => vec![Player, Queue],
        Subsystem::Database => vec![Library, Playlists],
        Subsystem::Mixer => vec![Volume],
        Subsystem::Playlist => vec![Playlists],
        Subsystem::Queue => vec![Queue],
        _ => vec![],
    }
    .into_iter()
    .map(|c| Update(c))
    .collect()
}

pub fn control_thread(app: Arc<App>) {
    let mut workers = Vec::with_capacity(app.config().event_threads as usize);

    for _ in 0..app.config().event_threads {
        let app = app.clone();

        workers.push(thread::spawn(move || loop {
            let rx = app.rx();
            if let Ok(message) = rx.recv() {
                std::mem::drop(rx);
                let _ = app.handle_message(message.clone());
                if app.has_quit() {
                    break;
                }
            }
        }));
    }

    workers.into_iter().for_each(|w| {
        let _ = w.join();
    });
}

pub fn mpd_listen_thread(app: Arc<App>) {
    let mut connection = MPDConnection::new(app.config());

    // This channel is used to break out of "idle_guard.get()"
    connection
        .subscribe(Channel::new("mpc_rs_msg").unwrap())
        .unwrap();

    let subsystems = get_subsytems();

    app.send_message(Message::Update(ComponentSelector::All));

    loop {
        let idle_guard = connection.idle(&subsystems).unwrap();

        if let Ok(subsystems) = idle_guard.get() {
            // When recieving a message, we must check if the
            // main thread is trying to join this thread
            let messages = connection.readmessages().unwrap();

            let mut stop = false;
            for message in messages {
                if &message.message[..] == "stop" {
                    stop = true
                }
            }

            // If so we break out of the loop
            if stop {
                break;
            }

            subsystems
                .into_iter()
                .map(|s| subsystem_messages(s))
                .flatten()
                .for_each(|message: Message| {
                    send_message(&app, message);
                });
        }
    }
}

pub fn input_thread(app: Arc<App>) {
    app.input_loop();
}

fn main() -> Result<(), Box<dyn Error>> {
    panic::set_hook(Box::new(panic_hook));
    log_init();

    let app = Arc::new(App::new());

    let mpd_listen = spawn_thread(app.clone(), mpd_listen_thread);
    let control = spawn_thread(app.clone(), control_thread);
    let input = spawn_thread(app.clone(), input_thread);

    app.render_loop()?;

    let mut connection = MPDConnection::new(app.config());
    let _ = connection.sendmessage(Channel::new("mpc_rs_msg").unwrap(), "stop");
    app.send_message(Message::Quit);
    mpd_listen.join().unwrap();
    control.join().unwrap();
    input.join().unwrap();

    Ok(())
}

fn spawn_thread(app: Arc<App>, func: fn(Arc<App>)) -> thread::JoinHandle<()> {
    thread::spawn(move || func(app))
}

fn send_message(app: &App, message: Message) {
    if message == Message::Quit {
        for _ in 0..app.config().event_threads {
            app.send_message(message.clone());
        }
    } else {
        app.send_message(message);
    }
}

fn panic_hook(info: &PanicInfo<'_>) {
    if cfg!(debug_assertions) {
        let location = info.location().unwrap();

        let msg = match info.payload().downcast_ref::<&'static str>() {
            Some(s) => *s,
            None => match info.payload().downcast_ref::<String>() {
                Some(s) => &s[..],
                None => "Box<Any>",
            },
        };

        let stacktrace: String = format!("{:?}", Backtrace::new()).replace('\n', "\n\r");

        println!(
            "{}thread '<unnamed>' panicked at '{}', {}\n\r{}",
            termion::screen::ToMainScreen,
            msg,
            location,
            stacktrace
        );
    }
}
