use crate::{
    model::PlaylistsModel,
    view::{TableView, View},
    AppBackend, ThemeConfig,
};

use mpd::Playlist;
use tui::{
    layout::{Constraint, Rect},
    terminal::Frame,
    widgets::Row,
};

pub struct PlaylistsView {
    table: TableView<Playlist>,
}

impl Default for PlaylistsView {
    fn default() -> Self {
        Self {
            table: TableView::new(
                "Playlists".into(),
                vec!["  Name"],
                vec![Constraint::Percentage(100)],
                Box::new(|list| {
                    let lines = parse_list(&list).into_iter();
                    Row::Data(Box::new(lines))
                }),
            ),
        }
    }
}

impl View<PlaylistsModel> for PlaylistsView {
    fn view(
        &mut self,
        playlists: &mut PlaylistsModel,
        theme: &ThemeConfig,
        f: &mut Frame<AppBackend>,
        area: Rect,
    ) {
        self.table.view(&mut playlists.table, theme, f, area);
    }
}

fn parse_list(list: &Playlist) -> Vec<String> {
    vec![list.name.clone()]
}
