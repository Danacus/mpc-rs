mod input;
mod library;
mod playlists;
mod progress_bar;
mod queue;
mod table;

use input::InputView;
use library::LibraryView;
use playlists::PlaylistsView;
use progress_bar::ProgressBarView;
use queue::QueueView;
use table::TableView;

use crate::{
    model::TabContent,
    util::{ui_util, ui_util::AppStyle},
    AppBackend, InputState, Model, ThemeConfig,
};

use tui::{
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    terminal::Frame,
    text::{Span, Spans},
    widgets::{Block, Borders, Tabs, Paragraph},
};

pub trait View<T> {
    fn view(&mut self, model: &mut T, theme: &ThemeConfig, f: &mut Frame<AppBackend>, area: Rect);
}

#[derive(Default)]
pub struct AppView {
    input: InputView,
    library: LibraryView,
    playlists: PlaylistsView,
    progress_bar: ProgressBarView,
    queue: QueueView,
}

impl View<Model> for AppView {
    fn view(
        &mut self,
        mut model: &mut Model,
        theme: &ThemeConfig,
        f: &mut Frame<AppBackend>,
        area: Rect,
    ) {
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .margin(0)
            .constraints(
                [
                    Constraint::Length(3),
                    Constraint::Min(0),
                    Constraint::Length(3),
                ]
                .as_ref(),
            )
            .split(area);

        let block = Block::default().style(Style::default().fg(Color::Black));
        f.render_widget(block, area);

        if let Some(error) = &model.error_message {
            self.render_message(error.to_string(), theme, f, chunks[0]);
        } else {
            self.render_tabs(&mut model, &theme, f, chunks[0]);
        }

        match &model.current_content() {
            TabContent::Library => self.library.view(&mut model.library, &theme, f, chunks[1]),
            TabContent::Playlists => {
                self.playlists
                    .view(&mut model.playlists, &theme, f, chunks[1])
            }
            TabContent::Queue => self.queue.view(&mut model.queue, &theme, f, chunks[1]),
        }

        match &model.input_state {
            InputState::TextInput => {
                self.input.view(&mut model.input, &theme, f, chunks[2]);
            }
            InputState::KeyCommands => {
                self.progress_bar
                    .view(&mut model.progress_bar, &theme, f, chunks[2]);
            }
        }
    }
}

impl AppView {
    pub fn new() -> Self {
        Self::default()
    }

    fn render_tabs(
        &mut self,
        model: &mut Model,
        theme: &ThemeConfig,
        f: &mut Frame<AppBackend>,
        area: Rect,
    ) {
        let mut titles = model
            .tabs
            .titles
            .iter()
            .map(|t| {
                Spans::from(Span::styled(
                    t.clone(),
                    ui_util::get_style(&theme, &AppStyle::TabTitle),
                ))
            })
            .collect::<Vec<_>>();

        {
            let sequencer = &model.keys.sequencer;

            if !sequencer.sequence().is_empty() {
                titles.push(Spans::from(format!("{}", sequencer)));
            }
        }

        if model.loading > 0 {
            titles.push(Spans::from("◌"));
        }

        let tabs = Tabs::new(titles)
            .block(Block::default().borders(Borders::ALL))
            .select(model.tabs.index)
            .style(ui_util::get_style(&theme, &AppStyle::Border))
            .highlight_style(Style::default().add_modifier(Modifier::BOLD));
        f.render_widget(tabs, area);
    }

    fn render_message(
        &mut self,
        message: String,
        theme: &ThemeConfig,
        f: &mut Frame<AppBackend>,
        area: Rect,
    ) {
        let span = Span::styled(
            message,
            ui_util::get_style(&theme, &AppStyle::TabTitle),
        );

        let message = Paragraph::new(span)
            .block(Block::default().borders(Borders::ALL))
            .style(ui_util::get_style(&theme, &AppStyle::Border));

        f.render_widget(message, area);
    }
}
