use crate::{
    model::InputModel,
    util::{ui_util, ui_util::AppStyle},
    view::View,
    AppBackend, ThemeConfig,
};

use tui::{layout::Rect, terminal::Frame, text::Span, widgets::Paragraph};

#[derive(Default)]
pub struct InputView;

impl View<InputModel> for InputView {
    fn view(
        &mut self,
        input: &mut InputModel,
        theme: &ThemeConfig,
        f: &mut Frame<AppBackend>,
        area: Rect,
    ) {
        let widget = Paragraph::new(Span::styled(
            input.buffer.clone(),
            ui_util::get_style(&theme, &AppStyle::ContentText),
        ))
        .block(ui_util::create_content_frame(&theme, input.name.clone()));

        f.render_widget(widget, area);
    }
}
