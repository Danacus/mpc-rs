use crate::{
    model::LibraryModel,
    view::{TableView, View},
    AppBackend, ThemeConfig,
};

use tui::{
    layout::{Constraint, Rect},
    terminal::Frame,
    widgets::Row,
};

pub struct LibraryView {
    table: TableView<String>,
}

impl Default for LibraryView {
    fn default() -> Self {
        Self {
            table: TableView::new(
                "Library".into(),
                vec!["  Name"],
                vec![Constraint::Percentage(100)],
                Box::new(|text| {
                    let lines = vec![text.clone()].into_iter();
                    Row::Data(Box::new(lines))
                }),
            ),
        }
    }
}

impl View<LibraryModel> for LibraryView {
    fn view(
        &mut self,
        library: &mut LibraryModel,
        theme: &ThemeConfig,
        f: &mut Frame<AppBackend>,
        area: Rect,
    ) {
        self.table.set_title(library.get_title());
        self.table.view(&mut library.table, theme, f, area);
    }
}
