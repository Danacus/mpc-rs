use crate::{
    model::QueueModel,
    view::{TableView, View},
    AppBackend, ThemeConfig,
};

use mpd::Song;
use time::Duration;
use tui::{
    layout::{Constraint, Rect},
    style::{Modifier, Style},
    terminal::Frame,
    widgets::Row,
};

pub struct QueueView {
    table: TableView<Song>,
}

impl Default for QueueView {
    fn default() -> Self {
        Self {
            table: TableView::new(
                "Queue".into(),
                vec!["  #", "Artist", "Title", "🕔"],
                vec![
                    Constraint::Percentage(10),
                    Constraint::Percentage(35),
                    Constraint::Percentage(35),
                    Constraint::Percentage(15),
                ],
                Box::new(|song| {
                    let lines = parse_tags(&song).into_iter();
                    Row::Data(Box::new(lines))
                }),
            ),
        }
    }
}

impl View<QueueModel> for QueueView {
    fn view(
        &mut self,
        queue: &mut QueueModel,
        theme: &ThemeConfig,
        f: &mut Frame<AppBackend>,
        area: Rect,
    ) {
        let current_song_name: String = queue
            .current_song
            .clone()
            .map(|s| s.file)
            .unwrap_or_else(|| "".into());

        self.table.set_view_row(Box::new(move |song| {
            let lines = parse_tags(&song).into_iter();
            if current_song_name == song.file {
                return Row::StyledData(
                    Box::new(lines),
                    Style::default().add_modifier(Modifier::BOLD),
                );
            }

            Row::Data(Box::new(lines))
        }));

        self.table.view(&mut queue.table, theme, f, area);
    }
}

fn parse_tags(song: &Song) -> Vec<String> {
    let unknown_artist = String::from("Unknown Artist");
    vec![
        song.place.map(|p| p.pos).unwrap_or_else(|| 0).to_string(),
        song.tags.get("Artist").unwrap_or(&unknown_artist).clone(),
        song.title.clone().unwrap_or_else(|| song.file.clone()),
        format_duration(&song.duration.unwrap_or_else(|| Duration::seconds(0))),
    ]
}

fn format_duration(duration: &Duration) -> String {
    if duration.num_hours() > 0 {
        format!(
            "{:02}:{:02}:{:02}",
            duration.num_hours(),
            duration.num_minutes() % 60,
            duration.num_seconds() % 60
        )
    } else {
        format!(
            "{:02}:{:02}",
            duration.num_minutes() % 60,
            duration.num_seconds() % 60
        )
    }
}
