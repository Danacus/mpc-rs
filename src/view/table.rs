use crate::{model::TableModel, util::ui_util, view::View, AppBackend, ThemeConfig};

use tui::{
    layout::{Constraint, Rect},
    style::{Modifier, Style},
    terminal::Frame,
    widgets::{Row, Table, TableState},
};

type ViewRowFn<T> = Box<dyn Fn(&T) -> Row<Box<dyn Iterator<Item = String>>> + Send>;

#[derive(Default)]
pub struct TableView<T> {
    state: TableState,
    view_row: Option<ViewRowFn<T>>,
    title: String,
    header: Vec<&'static str>,
    widths: Vec<Constraint>,
}

impl<T> TableView<T> {
    pub fn new(
        title: String,
        header: Vec<&'static str>,
        widths: Vec<Constraint>,
        view_row: ViewRowFn<T>,
    ) -> Self {
        Self {
            state: TableState::default(),
            view_row: Some(view_row),
            title,
            header,
            widths,
        }
    }

    pub fn set_title(&mut self, title: String) {
        self.title = title;
    }

    pub fn set_view_row(&mut self, view_row: ViewRowFn<T>) {
        self.view_row = Some(view_row);
    }
}

impl<T: Clone> View<TableModel<T>> for TableView<T> {
    fn view(
        &mut self,
        table: &mut TableModel<T>,
        theme: &ThemeConfig,
        f: &mut Frame<AppBackend>,
        area: Rect,
    ) {
        self.state.select(table.selected_index());
        let items: Vec<Row<Box<dyn Iterator<Item = String>>>> = table
            .iter()
            .filter_map(|item| self.view_row.as_ref().map(|f| f(&item.clone())))
            .collect();
        let items = Table::new(self.header.clone().into_iter(), items.into_iter())
            .style(ui_util::table_style(&theme))
            .block(ui_util::create_content_frame(&theme, self.title.clone()))
            .highlight_style(Style::default().add_modifier(Modifier::BOLD))
            .highlight_symbol("→ ")
            .widths(&self.widths);

        f.render_stateful_widget(items, area, &mut self.state);
    }
}
