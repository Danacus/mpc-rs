use crate::{
    model::ProgressBarModel,
    util::{ui_util, ui_util::AppStyle},
    view::View,
    AppBackend, ThemeConfig,
};

use tui::{layout::Rect, terminal::Frame, widgets::Gauge};

#[derive(Default)]
pub struct ProgressBarView;

impl View<ProgressBarModel> for ProgressBarView {
    fn view(
        &mut self,
        progress_bar: &mut ProgressBarModel,
        theme: &ThemeConfig,
        f: &mut Frame<AppBackend>,
        area: Rect,
    ) {
        let gauge = Gauge::default()
            .block(ui_util::create_content_frame(
                &theme,
                progress_bar.state().into(),
            ))
            .gauge_style(ui_util::get_style(&theme, &AppStyle::Player))
            .percent(progress_bar.get_progress());

        f.render_widget(gauge, area);
    }
}
