use crate::App;

#[derive(Debug, Clone)]
pub struct VolumeModel {
    volume: i8,
}

impl Default for VolumeModel {
    fn default() -> Self {
        Self::new()
    }
}

impl VolumeModel {
    pub fn new() -> Self {
        Self { volume: 0 }
    }
}

fn update_state_volume(app: &App, status: mpd::Status) {
    app.model().volume.volume = status.volume;
}

pub fn update(app: &App) {
    let mut connection = app.connection();
    let _ = connection
        .status()
        .map(|status| update_state_volume(app, status));
}

pub fn set_volume(app: &App, new_volume: i8) {
    let mut connection = app.connection();
    if let Ok(()) = connection.volume(new_volume) {
        app.model().volume.volume = new_volume;
    }
}

pub fn change_volume(app: &App, delta: i8) {
    let volume = app.model().volume.volume;
    set_volume(app, volume + delta);
}
