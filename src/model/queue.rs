use crate::{data_types::*, model::TableModel, App};
use mpd::Song;

#[derive(Debug, Clone)]
pub struct QueueModel {
    pub table: TableModel<Song>,
    pub current_song: Option<Song>,
}

impl Default for QueueModel {
    fn default() -> Self {
        Self::new()
    }
}

impl QueueModel {
    pub fn new() -> Self {
        Self {
            table: TableModel::new(),
            current_song: None,
        }
    }
}

pub fn play(app: &App, entry: &ListEntrySelector) {
    let selected = app.model().queue.table.index_from_selector(entry);

    // Lock on model is released, so we can lock connection
    let _ = app.connection().switch(selected as u32);
}

pub fn update(app: &App) {
    let (items, current_song) = {
        let mut connection = app.connection();
        let items = TableModel::with_items(connection.queue().unwrap_or_else(|_| Vec::new()));
        let current_song = connection.currentsong().ok().and_then(|s| s);
        (items, current_song)
    };

    let queue = &mut app.model().queue;
    queue.table = items;
    queue.current_song = current_song;
    queue.table.select_index(
        queue
            .current_song
            .clone()
            .and_then(|s| s.place)
            .map(|p| p.pos as usize),
    );
}
