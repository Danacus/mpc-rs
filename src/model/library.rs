use crate::{data_types::ListEntrySelector, model::TableModel, App, MPDConnection};
use mpd::{Query, Song, Term};

#[derive(Debug, Clone)]
struct Selection(usize, String);

#[derive(Debug, Clone)]
enum State {
    Artists,
    Artist(Selection),
    Album(Selection, Selection),
    Song(Selection, Selection, Selection),
}

use State::*;

#[derive(Debug, Clone)]
pub struct LibraryModel {
    pub table: TableModel<String>,
    state: State,
}

impl Default for LibraryModel {
    fn default() -> Self {
        Self::new()
    }
}

impl LibraryModel {
    pub fn new() -> Self {
        Self {
            table: TableModel::new(),
            state: State::Artists,
        }
    }

    pub fn get_title(&self) -> String {
        match self.state.clone() {
            Artists => "Artists".into(),
            Artist(artist) => format!("Artists → {}", artist.1),
            Album(artist, album) => format!("Artists → {} → {}", artist.1, album.1),
            Song(artist, album, song) => {
                format!("Artists → {} → {} → {}", artist.1, album.1, song.1)
            }
        }
    }

    fn get_next_state(&self, selector: &ListEntrySelector) -> Option<State> {
        if let Some(selected) = self.table.item_from_selector(selector) {
            let index = self.table.index_from_selector(selector);
            let state = self.state.clone();
            return Some(match state {
                Artists => Artist(Selection(index, selected.clone())),
                Artist(artist) => Album(artist, Selection(index, selected.clone())),
                Album(ar, al) => Song(ar, al, Selection(index, selected.clone())),
                Song(ar, al, so) => Song(ar, al, so),
            });
        }

        None
    }
}

fn enqueue(app: &App, replace: bool, selector: &ListEntrySelector) {
    let state = app.model().library.get_next_state(selector);

    let mut connection = app.connection();
    if let Some(next_state) = state {
        if replace {
            let _ = connection.clear();
        }
        queue_add(&mut connection, next_state);
        if replace {
            let _ = connection.play();
        }
    }
}

pub fn play(app: &App, selector: &ListEntrySelector) {
    enqueue(app, true, selector)
}

pub fn add_to_queue(app: &App, selector: &ListEntrySelector) {
    enqueue(app, false, selector)
}

fn queue_add(connection: &mut MPDConnection, next_state: State) {
    match next_state {
        Artists => {
            unreachable!()
        }
        Artist(artist) => queue_artist(connection, &artist.1),
        Album(_, album) => queue_album(connection, &album.1),
        Song(_, album, song) => queue_song(connection, &album.1, &song.1),
    };
}

fn queue_song(connection: &mut MPDConnection, album: &str, song: &str) {
    let mut query = Query::new();
    let query = query
        .and(Term::Tag("Title".into()), song)
        .and(Term::Tag("Album".into()), album);
    let _ = connection.findadd(query);
}

fn queue_album(connection: &mut MPDConnection, album: &str) {
    let _ = connection.findadd(Query::new().and(Term::Tag("Album".into()), album));
}

fn queue_artist(connection: &mut MPDConnection, artist: &str) {
    let _ = connection
        .list(
            &Term::Tag("Album".into()),
            Query::new().and(Term::Tag("Artist".into()), artist),
        )
        .map(|albums| {
            albums
                .iter()
                .for_each(|album| queue_album(connection, album));
        });
}

fn list_all(app: &App, term: &str) {
    let list = app
        .connection()
        .list(&mpd::Term::Tag(term.into()), &Query::new())
        .unwrap_or_else(|_| Vec::new());

    app.model().library.table = TableModel::with_items(list);
}

fn list(app: &App, term: &str, query_tag: &str, query_value: &str) {
    let mut query = Query::new();
    let query = query.and(mpd::Term::Tag(query_tag.into()), query_value);
    let list = app
        .connection()
        .list(&mpd::Term::Tag(term.into()), &query)
        .unwrap_or_else(|_| Vec::new());

    app.model().library.table = TableModel::with_items(list);
}

fn find_songs(app: &App, album_name: &str) {
    let mut query = Query::new();
    let query = query.and(mpd::Term::Tag("Album".into()), album_name.to_string());
    let mut list = app
        .connection()
        .find(&query, None)
        .unwrap_or_else(|_| Vec::new());

    list.sort_by_key(|song| {
        song.tags
            .get("Track")
            .cloned()
            .map(|t| t.parse::<u32>().unwrap())
    });

    app.model().library.table = TableModel::with_items(
        list.iter()
            .map(|song| song.clone().title.unwrap_or(song.file.clone()))
            .collect(),
    );
}

fn find_song(
    connection: &mut MPDConnection,
    album_name: String,
    song_name: String,
) -> Option<Song> {
    let mut query = Query::new();
    let query = query
        .and(Term::Tag("Title".into()), song_name)
        .and(Term::Tag("Album".into()), album_name);
    connection
        .find(query, None)
        .ok()
        .map(|songs| songs.first().unwrap().clone())
}

fn update_list(app: &App, state: &State) {
    match state {
        Artists => list_all(&app, "AlbumArtist"),
        Artist(artist) => list(&app, "Album", "Artist", &artist.1),
        Album(_, album) => find_songs(&app, &album.1),
        Song(_, _, song) => list(&app, "Title", "Title", &song.1),
    }
}

pub fn update(app: &App) {
    let state = app.model().library.state.clone();
    update_list(app, &state);
}

pub fn state_down(app: &App, selector: &ListEntrySelector) {
    let state = app.model().library.get_next_state(selector);
    if let Some(state) = state {
        update_list(app, &state.clone());

        app.model().library.state = state;
    }
}

pub fn state_up(app: &App) {
    let mut new_index = None;

    let next_state = match app.model().library.state.clone() {
        Artists => Artists,
        Artist(artist) => {
            new_index = Some(artist.0);
            Artists
        }
        Album(artist, album) => {
            new_index = Some(album.0);
            Artist(artist)
        }
        Song(artist, album, song) => {
            new_index = Some(song.0);
            Album(artist, album)
        }
    };

    update_list(app, &next_state);

    let mut library = &mut app.model().library;
    library.state = next_state;
    library.table.select_index(new_index);
}
