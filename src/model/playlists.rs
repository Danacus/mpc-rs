use crate::{data_types::ListEntrySelector, model::TableModel, App};
use mpd::Playlist;

#[derive(Debug, Clone)]
pub struct PlaylistsModel {
    pub table: TableModel<Playlist>,
}

impl Default for PlaylistsModel {
    fn default() -> Self {
        Self::new()
    }
}

impl PlaylistsModel {
    pub fn new() -> Self {
        Self {
            table: TableModel::default(),
        }
    }
}

pub fn play(app: &App, selector: &ListEntrySelector) {
    let mut connection = app.connection();

    connection
        .clear()
        .ok()
        .and_then(|_| {
            app.model()
                .playlists
                .table
                .item_from_selector(selector)
                .cloned()
        })
        .and_then(|playlist| connection.load(playlist, ..).ok())
        .and_then(|_| connection.play().ok());
}

pub fn save(app: &App, name: &str) {
    let _ = app.connection().save(name);
    update(app);
}

pub fn update(app: &App) {
    let playlists = app.connection().playlists();

    app.model().playlists.table = TableModel::with_items(playlists.unwrap_or_else(|_| Vec::new()));
}
