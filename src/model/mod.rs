pub mod input;
pub mod library;
pub mod playlists;
pub mod progress_bar;
pub mod queue;
pub mod table;
pub mod volume;

use serde::{Deserialize, Serialize};
use strum::IntoEnumIterator;
use strum_macros::{EnumIter, EnumString, IntoStaticStr};

use crate::{data_types::*, util::ui_util::TabsState, Config, Keys};

pub use input::InputModel;
pub use library::LibraryModel;
pub use playlists::PlaylistsModel;
pub use progress_bar::ProgressBarModel;
pub use queue::QueueModel;
pub use table::TableModel;
pub use volume::VolumeModel;

#[derive(
    Debug, Clone, EnumIter, EnumString, IntoStaticStr, PartialEq, Eq, Serialize, Deserialize,
)]
pub enum TabContent {
    Library,
    Playlists,
    Queue,
}

impl TabContent {
    pub fn input_context(&self) -> Vec<Context> {
        match self {
            TabContent::Library => vec![Context::Global, Context::Library, Context::List],
            TabContent::Playlists => vec![Context::Global, Context::Playlists, Context::List],
            TabContent::Queue => vec![Context::Global, Context::Queue, Context::List],
        }
    }

    pub fn index(&self) -> usize {
        match self {
            TabContent::Library => 0,
            TabContent::Playlists => 1,
            TabContent::Queue => 2,
        }
    }
}

#[derive(Debug)]
pub struct Model {
    pub queue: QueueModel,
    pub library: LibraryModel,
    pub playlists: PlaylistsModel,

    pub progress_bar: ProgressBarModel,
    pub volume: VolumeModel,
    pub input: InputModel,

    pub tabs: TabsState,

    pub keys: Keys,
    pub input_state: InputState,
    pub loading: u32,
    pub error_message: Option<String>,
}

impl Model {
    pub fn new(config: &Config) -> Self {
        Self {
            queue: QueueModel::new(),
            library: LibraryModel::new(),
            playlists: PlaylistsModel::new(),
            progress_bar: ProgressBarModel::new(),
            volume: VolumeModel::new(),
            input: InputModel::new(),
            tabs: create_tabs(),
            keys: Keys::new(&config),
            input_state: InputState::KeyCommands,
            error_message: None,
            loading: 0,
        }
    }

    pub fn current_content(&self) -> TabContent {
        TabContent::iter()
            .nth(self.tabs.index)
            .expect("The index of tabs should never be outside the range of tabs.")
    }
}

fn create_tabs() -> TabsState {
    TabsState::new(TabContent::iter().map(|t| t.into()).collect())
}
