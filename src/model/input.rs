use crate::data_types::*;

#[derive(Debug, Clone)]
pub struct InputModel {
    pub buffer: String,
    pub name: String,
    pub return_msg: Message,
}

impl Default for InputModel {
    fn default() -> Self {
        Self::new()
    }
}

impl InputModel {
    pub fn new() -> Self {
        Self {
            buffer: String::new(),
            name: String::new(),
            return_msg: Message::None,
        }
    }

    pub fn handle_message(&mut self, name: &str, message: &Message) {
        self.name = name.into();
        self.return_msg = message.clone();
    }

    pub fn clear(&mut self) {
        self.name = "".into();
        self.buffer = "".into();
    }

    pub fn type_char(&mut self, c: char) {
        self.buffer.push(c)
    }

    pub fn type_str(&mut self, s: &str) {
        self.buffer.push_str(s)
    }

    pub fn pop_char(&mut self) {
        self.buffer.pop();
    }
}
