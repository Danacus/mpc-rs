use crate::data_types::*;

#[derive(Debug, Clone)]
pub struct TableModel<T> {
    items: Vec<T>,
    selected: Option<usize>,
}

impl<T> Default for TableModel<T> {
    fn default() -> Self {
        Self {
            items: Vec::new(),
            selected: None,
        }
    }
}

impl<T> TableModel<T> {
    pub fn new() -> Self {
        Self {
            items: Vec::new(),
            selected: None,
        }
    }

    pub fn with_items(items: Vec<T>) -> Self {
        Self {
            items,
            selected: None,
        }
    }

    pub fn index_from_selector(&self, selector: &ListEntrySelector) -> usize {
        use ListEntrySelector::*;

        match selector {
            Next => self
                .selected_index()
                .map(|s| (s as isize + 1).rem_euclid(self.items.len() as isize))
                .unwrap_or(0) as usize,
            Prev => self
                .selected_index()
                .map(|s| (s as isize - 1).rem_euclid(self.items.len() as isize))
                .unwrap_or(0) as usize,
            First => 0,
            Last => self.items.len() - 1,
            Selected => self.selected_index().unwrap_or(0),
            Nth(n) => n.rem_euclid(self.items.len()),
        }
    }

    pub fn item_from_selector<'a>(&'a self, selector: &ListEntrySelector) -> Option<&'a T> {
        self.items.get(self.index_from_selector(selector))
    }

    pub fn iter(&self) -> std::slice::Iter<T> {
        self.items.iter()
    }

    pub fn select(&mut self, selector: &ListEntrySelector) {
        if !self.items.is_empty() {
            self.selected = Some(self.index_from_selector(selector));
        } else {
            self.selected = None;
        }
    }

    pub fn select_index(&mut self, index: Option<usize>) {
        self.selected = index;
    }

    pub fn selected_index(&self) -> Option<usize> {
        self.selected
    }

    pub fn selected(&self) -> Option<&T> {
        self.selected.and_then(|s| self.items.get(s))
    }
}
