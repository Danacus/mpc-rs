use crate::App;
use mpd::status::State;
use std::time::SystemTime;
use time::Duration;

#[derive(Debug, Clone)]
pub struct ProgressBarModel {
    state: String,
    duration: Duration,
    elapsed: Duration,
    last_update: SystemTime,
    playing: bool,
}

impl Default for ProgressBarModel {
    fn default() -> Self {
        Self::new()
    }
}

impl ProgressBarModel {
    pub fn new() -> Self {
        Self {
            state: "Connecting...".to_string(),
            duration: Duration::seconds(0),
            elapsed: Duration::seconds(0),
            last_update: SystemTime::now(),
            playing: false,
        }
    }

    pub fn state(&self) -> &str {
        &self.state
    }

    pub fn get_progress(&self) -> u16 {
        if self.duration.num_seconds() > 0 {
            let current_elapsed = if self.playing {
                self.last_update
                    .elapsed()
                    .unwrap_or_else(|_| std::time::Duration::from_secs(0))
                    .as_secs()
            } else {
                0
            };

            ((((current_elapsed + self.elapsed.num_seconds() as u64) as f32)
                / (self.duration.num_seconds() as f32))
                * 100.0) as u16
        } else {
            0
        }
        .max(0)
        .min(100)
    }
}

pub fn update(app: &App) {
    let mut connection = app.connection();

    if let Ok(status) = connection.status() {
        let mut progress_bar = &mut app.model().progress_bar;

        if let Ok(Some(song)) = connection.currentsong() {
            progress_bar.state = match status.state {
                State::Play | State::Pause => {
                    let text = match status.state {
                        State::Play => {
                            progress_bar.playing = true;
                            "Playing"
                        }
                        State::Pause => {
                            progress_bar.playing = false;
                            "Paused"
                        }
                        _ => unreachable!(),
                    };

                    let unknown_artist = String::from("Unknown Artist");
                    let song_text = format!(
                        "{} - {}",
                        song.tags.get("Artist").unwrap_or(&unknown_artist).clone(),
                        song.title.clone().unwrap_or_else(|| song.file.clone()),
                    );
                    format!("{}: {}", text, song_text)
                }
                State::Stop => "Stopped".to_string(),
            };

            if let (Some(elapsed), Some(duration)) = (status.elapsed, status.duration) {
                progress_bar.elapsed = elapsed;
                progress_bar.duration = duration;
            }
        } else {
            progress_bar.state = "Stopped".to_string();
            progress_bar.playing = false;
            progress_bar.elapsed = Duration::zero();
            progress_bar.duration = Duration::zero();
        }
    } else {
        app.model().progress_bar.state = "Error".into();
    }

    app.model().progress_bar.last_update = SystemTime::now();

    app.update_view();
}
