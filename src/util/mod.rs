pub mod event;
pub mod ui_util;

#[macro_use]
pub mod log;

#[macro_use]
pub mod keys;

use crate::config::Config;
use mpd::Client;
use std::ops::{Deref, DerefMut};

pub struct MPDConnection {
    connection: Client,
    host: String,
    port: String,
}

impl MPDConnection {
    pub fn new(config: &Config) -> Self {
        Self {
            connection: Self::connect(&config.host, &config.port),
            host: config.host.clone(),
            port: config.port.clone(),
        }
    }

    fn connect(host: &str, port: &str) -> Client {
        Client::connect(format!("{}:{}", host, port)).unwrap()
    }
}

impl Deref for MPDConnection {
    type Target = Client;

    fn deref(&self) -> &Client {
        &self.connection
    }
}

impl DerefMut for MPDConnection {
    fn deref_mut(&mut self) -> &mut Client {
        if self.connection.ping().is_err() {
            self.connection = Self::connect(&self.host, &self.port);
        }

        &mut self.connection
    }
}
