use crate::ThemeConfig;
use serde::{Deserialize, Serialize};
use std::i64;
use tui::{
    style::{Color, Style},
    text::Span,
    widgets::{Block, Borders},
};

pub fn color_from_hex(hex: &str) -> Color {
    let hex = i64::from_str_radix(hex.strip_prefix("#").expect("Invalid color"), 16).unwrap();

    let r = ((hex & 0xFF0000) >> 16) as u8;
    let g = ((hex & 0x00FF00) >> 8) as u8;
    let b = (hex & 0x0000FF) as u8;

    Color::Rgb(r, g, b)
}

pub fn theme_color(theme: &ThemeConfig, style: &AppStyle) -> Option<Color> {
    theme.get(&style).map(|hex| color_from_hex(hex))
}

#[derive(Serialize, Deserialize, Debug, Clone, Hash, PartialEq, Eq)]
pub enum AppStyle {
    Border,
    FrameTitle,
    TabTitle,
    ContentText,
    Player,
}

fn fg_style(theme: &ThemeConfig, style: &AppStyle) -> Style {
    Style::default().fg(theme_color(&theme, &style).unwrap_or(Color::White))
}

pub fn get_style(theme: &ThemeConfig, style: &AppStyle) -> Style {
    fg_style(theme, &style)
}

pub fn frame_title_span(theme: &ThemeConfig, title: String) -> Span {
    return Span::styled(title, get_style(theme, &AppStyle::FrameTitle));
}

pub fn create_content_frame(theme: &ThemeConfig, title: String) -> Block {
    Block::default()
        .title(frame_title_span(&theme, title))
        .borders(Borders::ALL)
        .border_style(get_style(&theme, &AppStyle::Border))
}

pub fn table_style(theme: &ThemeConfig) -> Style {
    get_style(&theme, &AppStyle::ContentText)
}

#[derive(Debug, Clone)]
pub struct TabsState {
    pub titles: Vec<String>,
    pub index: usize,
}

impl TabsState {
    pub fn new(titles: Vec<&str>) -> TabsState {
        TabsState {
            titles: titles.iter().map(|t| (*t).into()).collect(),
            index: 0,
        }
    }

    pub fn next(&mut self) {
        self.index = (self.index + 1) % self.titles.len();
    }

    pub fn previous(&mut self) {
        if self.index > 0 {
            self.index -= 1;
        } else {
            self.index = self.titles.len() - 1;
        }
    }

    pub fn set(&mut self, index: usize) {
        if index < self.titles.len() {
            self.index = index
        }
    }
}
