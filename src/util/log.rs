#[macro_export]
macro_rules! log {
    ($pattern:expr, $($val:expr),* $(,)?) => {{
        use std::io::prelude::*;
        let mut file = std::fs::OpenOptions::new().append(true).open("out.log").unwrap();
        let message = format!($pattern, $($val,)*);
        let text = format!("{:?}> {}\n", std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap(), message);
        let _ = file.write_all(text.as_bytes());
    }};
    ($text:expr) => {{
        use std::io::prelude::*;
        let mut file = std::fs::OpenOptions::new().append(true).open("out.log").unwrap();
        let text = format!("{:?}> {}\n", std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap(), $text);
        let _ = file.write_all(text.as_bytes());
    }};
}

pub fn log_init() {
    std::fs::write("out.log", "").unwrap();
}
