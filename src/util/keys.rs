use crate::config::Config;
use std::{collections::HashMap, fmt};
use strum::IntoEnumIterator;
use termion::event::Key;

use crate::{Context, Message};

pub type Action = (Context, String);
pub type KeyMap = HashMap<Context, HashMap<Vec<Key>, Message>>;
type FilteredKeyMap<'a> = HashMap<&'a str, HashMap<Vec<Key>, &'a str>>;

#[derive(Debug)]
pub struct KeySequencer {
    sequence: Vec<Key>,
    len: usize,
}

impl KeySequencer {
    pub fn new(len: usize) -> Self {
        Self {
            sequence: Vec::new(),
            len,
        }
    }

    pub fn reset(&mut self) {
        self.sequence.clear();
    }

    pub fn add(&mut self, key: Key) -> bool {
        if self.sequence.len() >= self.len - 1 {
            self.reset();
            false
        } else {
            self.sequence.push(key);
            true
        }
    }

    pub fn sequence(&self) -> &Vec<Key> {
        &self.sequence
    }

    pub fn current(&self) -> Option<&Key> {
        self.sequence.last()
    }

    pub fn pos(&self) -> usize {
        self.sequence.len() - 1
    }

    pub fn set_min_len(&mut self, len: usize) {
        if len > self.len {
            self.len = len
        }
    }
}

impl fmt::Display for KeySequencer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut res = String::new();

        for key in &self.sequence {
            res.push_str(&key_to_string(key));
            res.push(' ');
        }

        write!(f, "{}", res)
    }
}

#[derive(Debug)]
pub struct Keys {
    key_map: KeyMap,
    pub sequencer: KeySequencer,
}

impl Keys {
    pub fn get_message(&self, context: &[Context], key: Key) -> Option<Message> {
        let mut seq = self.sequencer.sequence().clone();

        for ctx in context {
            if let Some(message) = self.key_map.get(ctx).and_then(|map| {
                let mut res = None;

                seq.push(key);
                for i in 0..seq.len() {
                    if let Some(act) = map.get(&seq[i..]) {
                        res = Some(act);
                        break;
                    }
                }
                seq.pop();

                res
            }) {
                return Some(message.clone());
            }
        }

        None
    }

    pub fn default() -> Self {
        let mut key_map: KeyMap = HashMap::new();

        Context::iter().for_each(|ctx| {
            key_map.insert(ctx, HashMap::new());
        });

        Self {
            key_map,
            sequencer: KeySequencer::new(1),
        }
    }

    pub fn new(config: &Config) -> Self {
        let mut keys = Self::default();
        let sequencer = &mut keys.sequencer;

        for (ctx, map) in &config.keys {
            if let Some(ctx_map) = keys.key_map.get_mut(&ctx) {
                for (key, message) in map {
                    parse_key_sequence(key)
                        .map(|k| {
                            sequencer.set_min_len(k.len());
                            ctx_map.insert(k, message.clone());
                        })
                        .unwrap()
                }
            }
        }

        keys
    }

    // Used to notify this Keys struct whether or not the key was captured
    pub fn set_status(&mut self, status: bool, key: Key) {
        if status {
            // Key was captured, reset sequencer and restore filtered map
            self.sequencer.reset();
        } else {
            // Key was pressed, but not captured
            // Increment the sequencer
            self.sequencer.add(key);
        }
    }
}

fn parse_key_sequence(sequence: &str) -> Result<Vec<Key>, String> {
    sequence.split('+').map(|key| parse_key(key)).collect()
}

fn parse_key(key: &str) -> Result<Key, String> {
    fn get_single_char(string: &str) -> char {
        match string.chars().next() {
            Some(c) => c,
            None => panic!(),
        }
    }

    match key.len() {
        1 => Ok(Key::Char(get_single_char(key))),
        _ => {
            let sections: Vec<&str> = key.split('-').collect();

            if sections.len() > 2 {
                return Err(format!(
                    "A keybind can only have one modifier key, \"{}\" has {}",
                    key,
                    sections.len() - 1
                ));
            }

            match sections[0].to_lowercase().as_str() {
                "ctrl" => Ok(Key::Ctrl(get_single_char(sections[1]))),
                "alt" => Ok(Key::Alt(get_single_char(sections[1]))),
                "left" => Ok(Key::Left),
                "right" => Ok(Key::Right),
                "up" => Ok(Key::Up),
                "down" => Ok(Key::Down),
                "backspace" => Ok(Key::Backspace),
                "delete" => Ok(Key::Delete),
                "del" => Ok(Key::Delete),
                "esc" | "escape" => Ok(Key::Esc),
                "pageup" => Ok(Key::PageUp),
                "pagedown" => Ok(Key::PageDown),
                "space" => Ok(Key::Char(' ')),
                "enter" => Ok(Key::Char('\n')),
                "plus" => Ok(Key::Char('+')),
                "minus" => Ok(Key::Char('-')),
                _ => Err(format!("The key \"{}\" is unknown.", sections[0])),
            }
        }
    }
}

fn key_to_string(key: &Key) -> String {
    match key {
        Key::Ctrl(c) => {
            let mut string = String::from("ctrl-");
            string.push(*c);
            string
        }
        Key::Alt(c) => {
            let mut string = String::from("alt-");
            string.push(*c);
            string
        }
        Key::Left => "left".to_string(),
        Key::Right => "right".to_string(),
        Key::Up => "up".to_string(),
        Key::Down => "down".to_string(),
        Key::Backspace => "backspace".to_string(),
        Key::Delete => "delete".to_string(),
        Key::Esc => "esc".to_string(),
        Key::PageUp => "pageup".to_string(),
        Key::PageDown => "pagedown".to_string(),
        Key::Char(' ') => "space".to_string(),
        Key::Char('\n') => "enter".to_string(),
        Key::Char('+') => "plus".to_string(),
        Key::Char('-') => "minus".to_string(),
        Key::Char(c) => c.to_string(),
        _ => "".into(),
    }
}
