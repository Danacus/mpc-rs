# mpc-rs

Terminal based client for the Music Player Daemon written in Rust.

Sends a minimal amount of commands to MPD and automatically creates a new connection when the previous one times out
and a new connection is required. It also uses a separate idling connection to listen for relevant events from MPD (on a separate thread).  
tl;dr No polling for events, no pinging to keep connections alive, no creation of a new connection for each request,
like some other clients might do.

Roughly based on `vimpc`, as it also uses vi-like keybinds.

# Features (WIP)

- [x] Player status (WIP)
- [x] Player play/pause
- [x] View queue and play songs from queue
- [x] List and play playlists
- [x] Browse library (WIP) 
- [x] Configurable (keybinds, theme (soon), ...)
- [x] Drawing in separate thread such that reconnecting doesn't block the UI
- [ ] Any other basic feature you would expect from a MPD client


